clean:
	rm -fr dist/ *.eggs .eggs build/ .coverage htmlcov/ .mypy_cache/ .pytest_cache/ *.log *.egg-info
	find . -name '__pycache__' | xargs rm -rf
	find . -name '*.pyc' | xargs rm -rf

test:
	pip install '.[dev]'
	SECRET=123 python3 setup.py test

mypy:
	pip install mypy
	mypy --install-types --non-interactive ttbot

release: clean
	pip install twine
	python setup.py sdist bdist_wheel
	python3 -m twine upload dist/*
